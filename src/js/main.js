const city = document.querySelector(".weather-city");
const country = document.querySelector(".weather-country");
const theSky = document.querySelector(".weather-theSky");
const degrees = document.querySelector(".weather-degrees");
const humidity = document.querySelector(".weather-humidity");
const windSpeed = document.querySelector(".weather-windSpeed");
const celisius = document.querySelector(".celisius");
const skyLogo = document.querySelector(".weather-skyLogo");
const errMessage = document.querySelector(".weather-errInfo");
errMessage.innerHTML = "Processing data...";

//detect carousel-slide height
function blockHeight() {
  const blockHeight = document.querySelectorAll('.carousel-slide img')[1].clientHeight + 20;
  document.getElementById('carousel-slide').style.height = blockHeight + 'px';
}

//window resizing carousel-slide height
window.addEventListener('resize', blockHeight);
window.addEventListener('load', blockHeight);

//burger menu

document.addEventListener('DOMContentLoaded', () => {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(el => {
      el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }

});



//Carousel

const carouselSlide = document.getElementById('carousel-slide');  //get the block-slider
const carouselSlideImg = document.querySelectorAll('.carousel-slide img'); //get every image
//control Buttons
const prevBtn = document.querySelector(".prev");
const nextBtn = document.querySelector(".next");
//prev Button add event
prevBtn.addEventListener("click", functionalSlider);
//next Button add event
nextBtn.addEventListener("click", functionalSlider);

//function to slider
function functionalSlider(event) {
  let currentClasses = [];

  //read current img classes and write every second class in array
  for (let i = 0; i < carouselSlideImg.length; i++) {
    currentClasses.push(carouselSlideImg[i].classList[1]);
  }
  //remove current classes from every img
  for (let i = 0; i < carouselSlideImg.length; i++) {
    carouselSlideImg[i].classList.remove(currentClasses[i]);
  }

  //change class order in array
  if (event.target.classList.value == "prev") {
    const lastClass = currentClasses[currentClasses.length - 1];
    currentClasses.pop(lastClass);
    currentClasses.splice(0, 0, lastClass);
  } else if (event.target.classList.value == "next") {
    const firstClass = currentClasses[0];
    currentClasses.shift();
    currentClasses.push(firstClass);
  }

  //add every new classes to every img
  for (let i = 0; i < carouselSlideImg.length; i++) {
    carouselSlideImg[i].classList.add(currentClasses[i]);
  }
}


//Scroll to top

let arrowUp = document.querySelector(".foo-to-top a");

arrowUp.addEventListener("click", function () {
  window.scrollTo(0, 0);
});

//Scroll Down

let arrowDown = document.querySelector(".scroll-down");
let scrolling = document.querySelector(".scroll-to");

arrowDown.addEventListener("click", function () {
  scrolling.scrollIntoView();
});

//detect user Geolocation and show Weather for user

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition, err);
  } else {
    errMessage.innerHTML = "Geolocation is not supported by this browser.";
  }
} 

function showPosition(position) {
  let lat = position.coords.latitude;
  let lon = position.coords.longitude
  const appid = "358257d46dfcc2962fc380e7ffad5fda";

  fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${appid}`)
    .then(res => {
      if (res.status === 200) {
        return res.json();
      } else {
        throw errMessage.innerHTML ='Something went wrong on api server! We will use a default location';
      }
    })
    .then(showWeather)
}

function showWeather(res) {
  let temp;
  
  errMessage.innerHTML = "";
  city.innerHTML = res.name;
  country.innerHTML = res.sys.country;
  switch(res.weather[0].main.toLowerCase()) {
    case "clear":
      theSky.innerHTML = "SUNNY";
      skyLogo.src = "./img/sunny.png";
      break;
    case "clouds":
        theSky.innerHTML = "CLOUDY";
        skyLogo.src = "./img/cloudy.png";
        break;
    case "rain":
        theSky.innerHTML = "RAINY";
        skyLogo.src = "./img/rainy.png";
        break;
  }

  temp = kelvinToCelisius(res.main.temp);
  degrees.innerHTML = temp + "&#176";
  humidity.innerHTML += res.main.humidity + "%";
  windSpeed.innerHTML += Math.round(res.wind.speed) + " MPH";
}

function kelvinToCelisius(temp) {
  return Math.round(temp - 273.15);
}

function err(e) {
  switch(e.code) {
    case 1: 
      errMessage.innerHTML = "Your Browser haven't acces to your location. Please allow acces to location.";
      break;
    case 2:
      errMessage.innerHTML = "Location information is unavailable.";
      break;
    case 3:
      errMessage.innerHTML = "The request to get user location is timed out.";
      break;
    default:
      errMessage.innerHTML = "An unknown error occurred.";
  }
};
getLocation();

